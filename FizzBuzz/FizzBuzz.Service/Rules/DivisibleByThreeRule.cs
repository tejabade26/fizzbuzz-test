﻿namespace FizzBuzz.Service.Rules
{
    using System;
    using FizzBuzz.Service.Interface;
   
    public class DivisibleByThreeRule:IRule
    {
        private readonly IDayOfWeekRule weekRule;

        public DivisibleByThreeRule(IDayOfWeekRule weekRule)
        {
            this.weekRule = weekRule;
        }

        public bool CheckHandler(int number)
        {
            return number % 3 == 0;
        }
      
        public string GetResults()
        {
            return this.weekRule.CheckHandler(DateTime.Now.DayOfWeek) ? "Wizz" : "Fizz";
        }
    }
}
