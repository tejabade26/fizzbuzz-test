﻿namespace FizzBuzz.Service.Rules
{
    using System;
    using FizzBuzz.Service.Interface;
    
    public class DayOfWeekRule:IDayOfWeekRule
    {
        public bool CheckHandler(DayOfWeek dayOfWeek)
        {
            return dayOfWeek == DayOfWeek.Wednesday;
        }
    }
}
