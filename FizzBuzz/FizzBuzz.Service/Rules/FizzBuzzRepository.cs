﻿namespace FizzBuzz.Service.Rules
{
    using System.Collections.Generic;
    using System.Linq;
    using FizzBuzz.Service.Interface;
    
    public class FizzBuzzRepository:IFizzBuzzRepository
    {
        private readonly IRule[] rules;

        public FizzBuzzRepository(IRule[] iRule)
        {
            this.rules = iRule;
        }

        public List<string> GetResult(int userInput)
        {
            var fizzbuzzlist = new List<string>();

            for (int input = 1; input <= userInput; input++)
            {
                var matchNumberNumericHandler = this.rules.Where(x => x.CheckHandler(input)).ToList();

                if (matchNumberNumericHandler.Any())
                {
                    var matchnumberlist = string.Join(" ", matchNumberNumericHandler.Select(x => x.GetResults()));
                    fizzbuzzlist.Add(matchnumberlist);
                }
                else
                {
                    fizzbuzzlist.Add(input.ToString());
                }
            }

            return fizzbuzzlist;
        }
    }
}
