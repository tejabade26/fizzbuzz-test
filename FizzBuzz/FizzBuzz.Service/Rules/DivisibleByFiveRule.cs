﻿namespace FizzBuzz.Service.Rules
{
    using System;
    using FizzBuzz.Service.Interface;
    
    public class DivisibleByFiveRule : IRule
    {
        private readonly IDayOfWeekRule weekRule;

        public DivisibleByFiveRule(IDayOfWeekRule weekRule)
        {
            this.weekRule = weekRule;
        }

        public bool CheckHandler(int number)
        {
            return number % 5 == 0;
        }

        public string GetResults()
        {
            return this.weekRule.CheckHandler(DateTime.Now.DayOfWeek) ? "Wuzz" : "Buzz";
        }
    }
}
