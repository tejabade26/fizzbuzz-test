﻿namespace FizzBuzz.Service.Interface
{
    using System.Collections.Generic;

    public interface IFizzBuzzRepository
    {
        List<string> GetResult(int number);
    }
}
