﻿namespace FizzBuzz.Service.Interface
{
    using System;

    public interface IDayOfWeekRule
    {
        bool CheckHandler(DayOfWeek dayOfWeek);
    }
}
