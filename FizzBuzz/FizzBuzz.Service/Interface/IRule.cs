﻿namespace FizzBuzz.Service.Interface
{
    public interface IRule
    {
        bool CheckHandler(int number);

        string GetResults();
    }
}
