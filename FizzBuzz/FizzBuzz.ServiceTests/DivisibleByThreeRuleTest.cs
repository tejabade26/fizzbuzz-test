﻿namespace FizzBuzz.ServiceTests
{
    using System;
    using Moq;
    using NUnit.Framework;
    using FizzBuzz.Service.Interface;
    using FizzBuzz.Service.Rules;
    
    public class DivisibleByThreeRuleTest
    {
        private Mock<IDayOfWeekRule> dayOfWeekRuleMock;

        [SetUp]
        public void TestSetUp()
        {
            this.dayOfWeekRuleMock = new Mock<IDayOfWeekRule>();
        }

        [TestCase(3, true)]
        [TestCase(5, false)]
        public void When_NumberIsDivisibleByThree_CheckHandler_Should_Return_Result(int number, bool expectedResult)
        {
            // Arrange
            this.dayOfWeekRuleMock.Setup(t => t.CheckHandler(DateTime.Now.DayOfWeek)).Returns(expectedResult);
            var divisibleByThreeHandler = new DivisibleByThreeRule(this.dayOfWeekRuleMock.Object);

            // Act
            var actualResult = divisibleByThreeHandler.CheckHandler(number);

            // Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestCase(true, "Wizz")]
        [TestCase(false, "Fizz")]
        public void When_DayIsWednesdayAndDivisibleByThree_GetResults_Should_Return_Result(bool result, string expectedResult)
        {
            // Arrange
            this.dayOfWeekRuleMock.Setup(t => t.CheckHandler(DateTime.Now.DayOfWeek)).Returns(result);
            var divisibleByThreeHandler = new DivisibleByThreeRule(this.dayOfWeekRuleMock.Object);

            // Act
            var actualResult = divisibleByThreeHandler.GetResults();

            // Assert
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
