﻿namespace FizzBuzz.ServiceTests
{
    using System;
    using FizzBuzz.Service.Rules;
    using NUnit.Framework;

    public class DayOfWeekRuleTest
    {
        [TestCase(DayOfWeek.Wednesday, true)]
        [TestCase(DayOfWeek.Friday, false)]
        public void When_DayOfWeekIsWednesday_CheckHandler_Should_Return_Result(DayOfWeek isWednesday, bool expectedResult)
        {
            // Arrange
            var dayOfWeekHandler = new DayOfWeekRule();

            // Act
            var actualResult = dayOfWeekHandler.CheckHandler(isWednesday);

            // Assert
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
