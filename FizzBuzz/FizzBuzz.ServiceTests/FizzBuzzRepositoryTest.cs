﻿namespace FizzBuzz.ServiceTests
{
    using System.Collections.Generic;
    using NUnit.Framework;
    using Moq;
    using FizzBuzz.Service.Rules;
    using FizzBuzz.Service.Interface;
    
    public class FizzBuzzRepositoryTest
    {
        private FizzBuzzRepository fizzBuzzRepository;
        private Mock<IRule> divisibleByThreeRuleMock;
        private Mock<IRule> divisibleByFiveRuleMock;

        [SetUp]
        public void TestSetUp()
        {
            this.divisibleByThreeRuleMock = new Mock<IRule>();
            this.divisibleByThreeRuleMock.Setup(x => x.CheckHandler(It.Is<int>(i => i % 3 == 0))).Returns(true);

            this.divisibleByFiveRuleMock = new Mock<IRule>();
            this.divisibleByFiveRuleMock.Setup(x => x.CheckHandler(It.Is<int>(i => i % 5 == 0))).Returns(true);

            this.fizzBuzzRepository = new FizzBuzzRepository(new[]
            {
                this.divisibleByThreeRuleMock.Object,
                this.divisibleByFiveRuleMock.Object
            });
        }

        [TestCase(5)]
        public void GetResult_Should_Return_FizzBuzzList(int number)
        {
            // Arrange
            var expectedList = new List<string> { "1", "2", "Fizz", "4", "Buzz" };
            this.divisibleByThreeRuleMock.Setup(x => x.GetResults()).Returns("Fizz");
            this.divisibleByFiveRuleMock.Setup(x => x.GetResults()).Returns("Buzz");

            // Act
            var actualResult = this.fizzBuzzRepository.GetResult(number);

            // Assert
            Assert.AreEqual(actualResult.Count, 5);
            Assert.AreEqual(actualResult, expectedList);
        }
    }
}
