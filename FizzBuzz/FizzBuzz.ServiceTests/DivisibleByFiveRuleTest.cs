﻿namespace FizzBuzz.ServiceTests
{
    using System;
    using Moq;
    using NUnit.Framework;
    using FizzBuzz.Service.Interface;
    using FizzBuzz.Service.Rules;
   
    public class DivisibleByFiveRuleTest
    {
        private Mock<IDayOfWeekRule> dayOfWeekRuleMock;

        [SetUp]
        public void TestSetUp()
        {
            this.dayOfWeekRuleMock = new Mock<IDayOfWeekRule>();
        }

        [TestCase(5, true)]
        [TestCase(510, true)]
        [TestCase(3, false)]
        [TestCase(9, false)]
        public void When_NumberIsDivisibleByFive_CheckHandler_Should_Return_Result(int number, bool expectedResult)
        {
            // Arrange
            this.dayOfWeekRuleMock.Setup(t => t.CheckHandler(DateTime.Now.DayOfWeek)).Returns(expectedResult);
            var divisibleByFiveHandler = new DivisibleByFiveRule(this.dayOfWeekRuleMock.Object);

            // Act
            var actualResult = divisibleByFiveHandler.CheckHandler(number);

            // Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestCase(true, "Wuzz")]
        [TestCase(false, "Buzz")]
        public void When_DayIsWednesdayAndDivisibleByFive_GetResults_Should_Return_Result(bool result, string expectedResult)
        {
            // Arrange
            this.dayOfWeekRuleMock.Setup(t => t.CheckHandler(DateTime.Now.DayOfWeek)).Returns(result);
            var divisibleByFiveHandler = new DivisibleByFiveRule(this.dayOfWeekRuleMock.Object);

            // Act
            var actualResult = divisibleByFiveHandler.GetResults();

            // Assert
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
