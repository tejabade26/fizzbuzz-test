﻿namespace FizzBuzz.Controllers
{
    using System;
    using System.Web.Mvc;
    using FizzBuzz.Service.Interface;
    using FizzBuzz.Models;
    using PagedList;

    public class FizzBuzzController : Controller
    {
        private readonly IFizzBuzzRepository repository;
        private int pageSize = 20;
        private int pageNumber;

        public FizzBuzzController(IFizzBuzzRepository repository)
        {
            this.repository = repository;
        }

        [HttpGet]
        public ActionResult Home(string userInput, int? page)
        {
            if (!string.IsNullOrEmpty(userInput))
            {
                var number = Convert.ToInt32(userInput);
                var resultmodel = this.GetFizzBuzzResults(number, page);
                return View("Home", resultmodel);
            }

            return View("Home");
        }

        [HttpPost]
        public ActionResult Home(FizzBuzzModel model, int? page)
        {
            if (ModelState.IsValid)
            {
                var resultmodel = GetFizzBuzzResults(model.UserInput, page);
                return View("Home", resultmodel);
            }

            return this.View("Home");
        }

        private FizzBuzzModel GetFizzBuzzResults(int userInput, int? page)
        {
            pageNumber = page.HasValue ? Convert.ToInt32(page) : 1;
            var result = this.repository.GetResult(userInput);

            var model = new FizzBuzzModel
            {
                UserInput = userInput,
                Result = result.ToPagedList(pageNumber, pageSize)
            };

            return model;
        }
    }
}
