namespace FizzBuzz 
{
    using StructureMap;
    using FizzBuzz.Service.Interface;
    using FizzBuzz.Service.Rules;

    public static class IoC 
    {
        public static IContainer Initialize() 
        {
            ObjectFactory.Initialize(x =>
                        {
                            x.Scan(scan =>
                                    {
                                        scan.TheCallingAssembly();
                                        scan.WithDefaultConventions();
                                    });
                            x.For<IRule>().AddInstances(y =>
                            {
                                y.Type<FizzBuzz.Service.Rules.DivisibleByThreeRule>();
                                y.Type<FizzBuzz.Service.Rules.DivisibleByFiveRule>();
                            });

                            x.For<IFizzBuzzRepository>().Use<FizzBuzzRepository>();
                            x.For<IDayOfWeekRule>().Use<DayOfWeekRule>();
                        });
           
            return ObjectFactory.Container;
        }
    }
}