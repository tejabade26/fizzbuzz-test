﻿namespace FizzBuzz.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using PagedList;
    
    public class FizzBuzzModel
    {
        [Required(ErrorMessage = "Please Enter a number")]
        [Display(Name = "Enter User Input")]
        [Range(1, 1000, ErrorMessage = "Please Enter number between 1 and 1000")]
        public int UserInput { get; set; }

        public IPagedList<string> Result { get; set; }
    }
}