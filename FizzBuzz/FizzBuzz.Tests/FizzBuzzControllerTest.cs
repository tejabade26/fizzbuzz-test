﻿namespace FizzBuzz.Tests
{
    using System.Web.Mvc;
    using System.Collections.Generic;
    using NUnit.Framework;
    using Moq;
    using FizzBuzz.Controllers;
    using FizzBuzz.Models;
    using FizzBuzz.Service.Interface;
   
    public class FizzBuzzControllerTest
    {
        private Mock<IFizzBuzzRepository> fizzBuzzRepositoryMock;

        [SetUp]
        public void TestSetUp()
        {
            this.fizzBuzzRepositoryMock = new Mock<IFizzBuzzRepository>();
        }

        [Test]
        public void WhenPageisLoaded_Home_ShouldReturn_View()
        {
            // Arrange
            var controller = new FizzBuzzController(this.fizzBuzzRepositoryMock.Object);
            var fizzbuzzModel = new FizzBuzzModel();

            // Act
            var actualResult = controller.Home(string.Empty, 1) as ViewResult;

            // Assert
            Assert.IsNotNull(actualResult);
            Assert.AreEqual("Home", actualResult.ViewName);
        }

        [Test]
        public void WhenUserEnterNumberAndSubmit_Home_ShouldReturn_NumberList()
        {
            // Arrange
            var controller = new FizzBuzzController(this.fizzBuzzRepositoryMock.Object);
            var fizzbuzzModel = new FizzBuzzModel();

            // Act
            var expectedList = new List<string> { "1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "Fizz Buzz" };
            this.fizzBuzzRepositoryMock.Setup(x => x.GetResult(15)).Returns(expectedList);
            var actualResult = controller.Home("15", 1) as ViewResult;

            // Assert
            Assert.IsNotNull(actualResult);
            var data = actualResult.ViewData.Model as FizzBuzzModel;
            Assert.AreEqual(data.UserInput, 15);
            Assert.AreEqual(data.Result[0].ToString(), "1");
            Assert.AreEqual(data.Result[2].ToString(), "Fizz");
            Assert.AreEqual(data.Result[4].ToString(), "Buzz");
            Assert.AreEqual(data.Result[14].ToString(), "Fizz Buzz");
        }

        [Test]
        public void WhenNextButtonClicked_Home_ShouldReturn_NumberList()
        {
            // Arrange
            var controller = new FizzBuzzController(this.fizzBuzzRepositoryMock.Object);
            var expectedList = new List<string> { "1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "Fizz Buzz", "16", "17", "Fizz", "19", "Buzz", "Fizz", "22", "23", "Fizz", "Buzz" };
            this.fizzBuzzRepositoryMock.Setup(x => x.GetResult(25)).Returns(expectedList);
            var fizzbuzzModel = new FizzBuzzModel();

            // Act
            var actualResult = controller.Home("25", 2) as ViewResult;

            // Assert
            var data = actualResult.ViewData.Model as FizzBuzzModel;
            Assert.AreEqual(data.UserInput, 25);
            Assert.AreEqual(data.Result[0].ToString(), "Fizz");
            Assert.AreEqual(data.Result[1].ToString(), "22");
            Assert.AreEqual(data.Result[2].ToString(), "23");
            Assert.AreEqual(data.Result[3].ToString(), "Fizz");
            Assert.AreEqual(data.Result[4].ToString(), "Buzz");
        }

        

    }
}
